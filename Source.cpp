#include<iostream>
#include<time.h>
#include<cstdlib>
#include<vector>
#include"header.h"
using namespace std;

void swap(vector<customer>& customers); //swap max and min

int main() {
	srand(time(0));
	vector<customer> customers; //keep customers value
	vector<cashier> cashiers; //keep cashiers value

	int MaxCustomers; //created for collect max customer data that possible
	int MinCustomers; //created for collect min customer data that possible
	int MaxServiceTime; //created for collect max service data that possible
	int MinServiceTime; //created for collect min service data that possible
	int NumCashiers; //created for collect number of cashiers data that possible

	cout << "MaxCustomers: ";
	cin >> MaxCustomers;

	cout << "MinCustomers: ";
	cin >> MinCustomers;

	cout << "MaxServiceTime: ";
	cin >> MaxServiceTime;

	cout << "MinServiceTime: ";
	cin >> MinServiceTime; 

	cout << "NumCashiers: ";
	cin >> NumCashiers;
	

	//create customer members
	int numCus = rand() % (MaxCustomers - MinCustomers + 1) + MinCustomers;
	for (int i = 0; i < numCus; i++) {
		customer one;
		one.arrive_t = rand()%241;//random 0-240
		one.service_t = rand()%(MinServiceTime-MaxServiceTime) + MinServiceTime;//random MinServiceTime-MaxServiceTime
		one.wait_t = 0;
		one.left_t = 0;
		customers.push_back(one); //add one to be member in customers
	}

	//create cashiers
	for (int i = 0; i < NumCashiers; i++) {
		cashier one;
		one.left_t = 0;
		cashiers.push_back(one); //add one to be member in cashiers
	}

	//experience of each customer
	swap(customers); //sort customers
	int i;
	for (i = 0; i < NumCashiers; i++) {
		customers[i].wait_t = 0;
		cashiers[i].left_t = customers[i].arrive_t + customers[i].service_t + customers[i].wait_t;
		customers[i].left_t = cashiers[i].left_t;
	}
	for (int j = i; j < numCus; j++) {
		int cashNo, wait;
		//cashier no.0
		if (cashiers[0].left_t<customers[j].arrive_t) {
			wait = 0;
		}
		else {
			wait = cashiers[0].left_t - customers[j].arrive_t;
		}
		cashNo = 0;
		//other cashier
		for (int c = 0; c < NumCashiers; c++) {
			if (cashiers[c].left_t < customers[j].arrive_t) {
				wait = 0;
				cashNo = c;
			}
			else if(wait> cashiers[c].left_t - customers[j].arrive_t){
				wait = cashiers[c].left_t - customers[j].arrive_t;
				cashNo = c;
			}
		}

		//set value
		customers[j].wait_t = wait;
		customers[j].left_t= customers[j].arrive_t + customers[j].service_t + customers[j].wait_t;
		cashiers[cashNo].left_t = customers[j].left_t;
	}

	//show customer experience
	for (int i = 0; i < numCus; i++) {
		cout << "\ncustomer " << i + 1;
		cout << " -arrive ";
		cout<<customers[i].arrive_t;
		cout << " -service ";
		cout<<customers[i].service_t;
		cout << " -wait ";
		cout<<customers[i].wait_t;
		cout << " -left ";
		cout<<customers[i].left_t;

	}

	//average wait tme
	float avg = 0;
	for (int i = 0; i < numCus; i++) {
		avg += customers[i].wait_t;
	}
	cout << "\n\naverage wait time " << avg / numCus;

	return 0;
}

void swap(vector<customer>& customers) {
	int i, j;
	for (i = 0; i < customers.size() - 1; i++) {
		//Last i elements are already in place  
		for (j = 0; j < customers.size() - i - 1; j++) {
			if (customers[j].arrive_t > customers[j + 1].arrive_t) {
				customer tmp = customers[j];
				customers[j] = customers[j + 1];
				customers[j + 1] = tmp;
			}
		}			
	}			
}